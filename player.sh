#!/bin/sh

echo "what clip do you want to preview?"
read clipname

exec mplayer -cache 20048 -fixed-vo $(cat $clipname)
