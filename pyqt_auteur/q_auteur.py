#! /usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
##                                                                           ##
##  Copyright 2010, Neil Wallace <rowinggolfer@googlemail.com>               ##
##                                                                           ##
##  This program is free software: you can redistribute it and/or modify     ##
##  it under the terms of the GNU General Public License as published by     ##
##  the Free Software Foundation, either version 3 of the License, or        ##
##  (at your option) any later version.                                      ##
##                                                                           ##
##  This program is distributed in the hope that it will be useful,          ##
##  but WITHOUT ANY WARRANTY; without even the implied warranty of           ##
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            ##
##  GNU General Public License for more details.                             ##
##                                                                           ##
##  You should have received a copy of the GNU General Public License        ##
##  along with this program.  If not, see <http://www.gnu.org/licenses/>.    ##
##                                                                           ##
###############################################################################

import os, sys
from optparse import OptionParser    

class Parser(OptionParser):
    def __init__(self):
        OptionParser.__init__(self)
        
        self.add_option("-e", "--edit", 
                        dest = "edit_filename",
                        help = "edit FILE", 
                        metavar="FILE")

        self.add_option("-p", "--play", 
                        dest = "play_filename",
                        help = "play FILE with mplayer",
                        metavar="FILE")
                        
        self.add_option("-r", "--render", 
                        dest = "render_filename",
                        help = "render FILE with mencoder",
                        metavar="FILE")

        self.add_option("-o", "--output_file",
                        help = "output (ie. save final video) to FILE",
                        metavar="FILE")
                        
        self.add_option("-m", "--mplayer",
                        dest = 'mplayer',
                        help = ("add mplayer options "
                                "(escaping whitespace if necessary) " 
                                "eg. ~$ q_auteur -m '-vo vaapi:gl' "
                                "or  ~$ q_auteur -m -nolirc"),
                        metavar="STRING")
        
        self.add_option("-M", "--mencoder",
                        dest = 'mencoder',
                        help = ("add mencoder (rendering) options "
                                "eg ~$ q_auteur -M '-vf scale=320:240'"),
                        metavar="STRING")
                        
    def parse_args(self):
        '''
        re-implement to ensure that
        "./q_auteur foo.xml" is the equivalent of
        "./q_auteur --edit foo.xml"
        '''
        options, args = OptionParser.parse_args(self)
        if args != [] and options.edit_filename == None:
            options.edit_filename = args.pop(0)
        if args != []:
            self.error("incorrect number of arguments")
        return options, args
    
def main():
    '''
    entry point of application
    '''
    import gettext
    gettext.install("q_auteur")
    
    try:
        raw_input
        print ("using Python2")
    except NameError:
        print ("using Python3")
    
    
    #make the script work from any directory
    def determine_path ():
        """Borrowed from wxglade.py"""
        try:
            root = __file__
            if os.path.islink (root):
                root = os.path.realpath (root)
            retarg = os.path.dirname (os.path.abspath (root))
            return retarg
        except:
            print("I'm sorry, but something is wrong.")
            print("There is no __file__ variable.")
            print("are you running this script from an IDE?")
            sys.exit ()

    wkdir = determine_path()
    sys.path.insert(0, os.path.dirname(wkdir))
    
    parser  = Parser()
    options, args = parser.parse_args()

    mplayer_opts = []
    if options.mplayer:
        mplayer_opts = options.mplayer.split(" ")
        print("User added mplayer options", mplayer_opts)
    
    mencoder_opts = ""
    if options.mencoder:
        mencoder_opts = options.mencoder
        print("User added mencoder options", mencoder_opts)
    
    if options.play_filename:
        from lib_q_auteur import player
        player.main(options.play_filename, mplayer_opts)
        
    elif options.render_filename:
        from lib_q_auteur import renderer
        renderer.main(options.render_filename, options.output_file, 
            mencoder_opts)

    else:
        from lib_q_auteur import editor
        editor.main(options.edit_filename, mplayer_opts, mencoder_opts)
        
        
if __name__ == "__main__":
    
    main()