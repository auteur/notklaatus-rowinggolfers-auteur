#! /usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
##                                                                           ##
##  Copyright 2010, Neil Wallace <rowinggolfer@googlemail.com>               ##
##                                                                           ##
##  This program is free software: you can redistribute it and/or modify     ##
##  it under the terms of the GNU General Public License as published by     ##
##  the Free Software Foundation, either version 3 of the License, or        ##
##  (at your option) any later version.                                      ##
##                                                                           ##
##  This program is distributed in the hope that it will be useful,          ##
##  but WITHOUT ANY WARRANTY; without even the implied warranty of           ##
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            ##
##  GNU General Public License for more details.                             ##
##                                                                           ##
##  You should have received a copy of the GNU General Public License        ##
##  along with this program.  If not, see <http://www.gnu.org/licenses/>.    ##
##                                                                           ##
###############################################################################

from lib_q_auteur.components import DataModel
import subprocess

class Player(object):
    def __init__(self, filename, mplayer_opts=[]):
        self.model = DataModel()
        self.model.load(filename)
        self.mplayer_opts = mplayer_opts
        
    def play(self):
        commands = ["mplayer", "-really-quiet", '-fixed-vo']
        commands.extend(self.mplayer_opts)
        additional_args = []
        for clip in self.model.clips:
            commands.append(clip.source_file)
            if clip.start:
                additional_args.extend([ "-ss", clip.start_format])
            if clip.end:
                additional_args.extend(["-endpos", clip.end_format])
            
        print("player command")
        print("="*80)
        readable_command = ""
        for command in commands:
            readable_command += command+" "
        print (readable_command)
        print("="*80)
        subprocess.Popen(commands)


def main(filename, mplayer_opts=[]):
    print("play", filename)
    player = Player(filename, mplayer_opts)
    player.play()
    
    
if __name__ == "__main__":
    main("components/test_video/test_project.xml")
    
    
    