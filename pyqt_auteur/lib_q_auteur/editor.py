#! /usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
##                                                                           ##
##  Copyright 2010, Neil Wallace <rowinggolfer@googlemail.com>               ##
##                                                                           ##
##  This program is free software: you can redistribute it and/or modify     ##
##  it under the terms of the GNU General Public License as published by     ##
##  the Free Software Foundation, either version 3 of the License, or        ##
##  (at your option) any later version.                                      ##
##                                                                           ##
##  This program is distributed in the hope that it will be useful,          ##
##  but WITHOUT ANY WARRANTY; without even the implied warranty of           ##
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            ##
##  GNU General Public License for more details.                             ##
##                                                                           ##
##  You should have received a copy of the GNU General Public License        ##
##  along with this program.  If not, see <http://www.gnu.org/licenses/>.    ##
##                                                                           ##
###############################################################################

import os, sys

from PyQt4 import QtCore, QtGui 

from lib_q_auteur.components import *

class Settings(object):
    '''
    default settings
    '''
    def __init__(self):
        self.toolbar_style = True
        self.is_docked = True    #docked as opposed to multiple windows
        
        #which widgets are visible?
        self.show_video = True
        
class MainApp(QtGui.QMainWindow, EditorMenu):
    def __init__(self, mplayer_opts=[], mencoder_opts=[], parent=None):
        QtGui.QMainWindow.__init__(self, parent)
        EditorMenu.__init__(self)
                
        self.setWindowTitle("q_auteur powered by mplayer/mencoder")
        
        self.mplayer_opts = mplayer_opts
        self.mencoder_opts = mencoder_opts
        
        self.addToolBar(self.tool_bar)
        
        ## the central widget
        self.screen_widget = ScreenWidget(mplayer_opts, self)
        self.player = self.screen_widget.player
        self.video_controls = VideoControls()
        self.video_controls.setObjectName("video_controls") #for QSettings

        ##some (optional) connections
        #self.player.set_label(self.video_controls.label)
        self.screen_widget.set_controls(self.video_controls)

        frame = QtGui.QFrame(self)
        layout = QtGui.QVBoxLayout(frame)
        layout.addWidget(self.screen_widget)
        layout.addWidget(self.video_controls)
        self.setCentralWidget(frame)
        
        ## the data model
        self.data_model = DataModel()
        
        ## data view widgets
        self.log_widget = LogWidget()
        self.log_widget.set_model(self.data_model)
        
        self.clips_widget = ClipsWidget()
        self.clips_widget.set_model(self.data_model)
        self.clips_widget.set_mencoder_opts(self.mencoder_opts)
        
        ## let the model now who's watching it!
        self.data_model.attach_view(self.clips_widget)
        self.data_model.attach_view(self.log_widget)
        self.data_model.attach_view(self) #to enable undo action
        
        self.connect_menu_signals()
        self.connect_widget_signals()
        self.SETTINGS = Settings()
        
        QtCore.QTimer.singleShot(1, self.loadSettings)
        
    def apply_settings(self):
        '''
        apply users gui preferences
        '''
        self.screen_widget.setVisible(self.SETTINGS.show_video)
        dock = DockedLogWidget(self.log_widget)
        dock.setObjectName("log_dock")
        self.addDockWidget(QtCore.Qt.RightDockWidgetArea, dock)
        
        dock = DockedClipsWidget(self.clips_widget)
        dock.setObjectName("clips_dock")
        self.addDockWidget(QtCore.Qt.BottomDockWidgetArea, dock)
        
    def connect_menu_signals(self):
        self.action_new_workspace.triggered.connect(self.new_workspace)
        self.action_load_workspace.triggered.connect(self.load_workspace)
        self.action_save_workspace.triggered.connect(self.save_workspace)        
        
        self.action_get_sources.triggered.connect(self.load_sources)
        
        self.action_undo.triggered.connect(self.undo)
        self.action_redo.triggered.connect(self.redo)
        
        self.connect(self.action_quit, QtCore.SIGNAL("triggered()"),
        QtGui.QApplication.instance().closeAllWindows)

        self.action_fullscreen.triggered.connect(self.fullscreen)

        self.action_about.triggered.connect(self.show_about)
        self.action_license.triggered.connect(self.show_license)

        self.connect(self.action_about_qt, QtCore.SIGNAL("triggered()"),
            QtGui.qApp, QtCore.SLOT("aboutQt()"))

        self.action_help.triggered.connect(self.show_help)

    def connect_widget_signals(self):
        ## signals from the clip widget
        self.connect(self.clips_widget, QtCore.SIGNAL("clip selected"),
            self.screen_widget.load_clip)
        self.connect(self.clips_widget, QtCore.SIGNAL("delete clip"),
            self.delete_clip)
        
        ## signals from the controls        
        self.connect(self.video_controls.label, QtCore.SIGNAL("split position"), 
            self.split_clip)
            
    def split_clip(self, time_stamp):
        selected_clip = self.clips_widget.selected_clip
        if selected_clip:
            self.screen_widget.stop()
            clip_id = self.data_model.split_clip(selected_clip, time_stamp)
            print(clip_id)
            self.clips_widget.select_clip_from_id(clip_id)
    
    def delete_clip(self):
        selected_clip = self.clips_widget.selected_clip
        if selected_clip:
            self.screen_widget.stop()
            self.data_model.delete_clip(selected_clip)
            
    def loadSettings(self):
        settings = QtCore.QSettings()
        
        try:
            self.video_path = settings.value("video_path").toString()
            self.workspace_path = settings.value("workspace_path").toString()
            
            self.SETTINGS.toolbar_style = settings.value("toolbar_style").toBool()
            self.SETTINGS.is_docked = settings.value("is_docked").toBool()

            self.apply_settings()
            self.restoreGeometry(settings.value("geometry").toByteArray())
            self.restoreState(settings.value("windowState").toByteArray())
        except AttributeError: #python3?
            self.video_path = settings.value("video_path")
            self.workspace_path = settings.value("workspace_path")
            
            self.SETTINGS.toolbar_style = settings.value("toolbar_style")
            self.SETTINGS.is_docked = settings.value("is_docked")

            self.apply_settings()
            self.restoreGeometry(settings.value("geometry"))
            self.restoreState(settings.value("windowState"))
            
            
    def saveSettings(self):
        settings = QtCore.QSettings()
        
        settings.setValue("geometry", self.saveGeometry())
        settings.setValue("windowState", self.saveState())
        settings.setValue("video_path", self.video_path)
        settings.setValue("workspace_path", self.workspace_path)        
        
        settings.setValue("toolbar_style", self.SETTINGS.toolbar_style)
        settings.setValue("is_docked", self.SETTINGS.is_docked)

    def show_toolbar(self):
        if self.action_show_toolbar.isChecked():
            self.video_controls.show()
        else:
            self.video_controls.hide()
        self.menu_toolbar.setEnabled(self.video_controls.isVisible())

    def show_about(self):
        self.reimplement_needed('show_about')

    def show_help(self):
        self.reimplement_needed('show_help')

    def show_license(self):
        '''
        attempts to read and show the license text
        from file COPYRIGHT.txt in the apps directory
        on failure, gives a simple message box with link.
        '''
        message = '''
        GPLv3 - see <a href='http://www.gnu.org/licenses/gpl.html'>
        http://www.gnu.org/licenses/gpl.html</a>'''
        try:
            f = open("../COPYING.txt")
            data = f.read()
            f.close()

            dl = QtGui.QDialog(self)
            dl.setWindowTitle(_("License"))
            dl.setFixedSize(400, 400)

            layout = QtGui.QVBoxLayout(dl)

            buttonBox = QtGui.QDialogButtonBox(dl)
            buttonBox.setOrientation(QtCore.Qt.Horizontal)
            buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Ok)

            te = QtGui.QTextBrowser()
            te.setText(data)

            label = QtGui.QLabel(message)
            label.setWordWrap(True)

            layout.addWidget(te)
            layout.addWidget(label)
            layout.addWidget(buttonBox)

            buttonBox.accepted.connect(dl.accept)

            dl.exec_()
        except IOError:
            QtGui.QMessageBox.information(self, _("License"), message)

    def fullscreen(self):
        if self.action_fullscreen.isChecked():
            self.setWindowState(QtCore.Qt.WindowFullScreen)
        else:
            self.setWindowState(QtCore.Qt.WindowNoState)

    def closeEvent(self, event=None):
        '''
        re-implement the close event of QtGui.QMainWindow, and check the user
        really meant to do this.
        '''
        if (
        self.data_model.is_dirty and 

        QtGui.QMessageBox.question(self, _("Confirm"), 
        _("You have unsaved changes to the workbench xml") + "<hr />" +
        _("Are you sure you want to quit?"),
        QtGui.QMessageBox.Yes | QtGui.QMessageBox.No,
        QtGui.QMessageBox.Yes) == QtGui.QMessageBox.No
        ):
            event.ignore()
        else:
            self.saveSettings()

    def load_sources(self, *args):
        filenames = QtGui.QFileDialog.getOpenFileNames(self,
        "select video sources", self.video_path, "video files (*.*)")
        QtGui.QApplication.instance().setOverrideCursor(QtCore.Qt.WaitCursor)
        for filename in filenames:
            filename = str(filename) 
            self.video_path = os.path.dirname(filename)
            
            self.data_model.add_source_file(filename)
            QtGui.QApplication.instance().processEvents()
        QtGui.QApplication.instance().restoreOverrideCursor()

    def new_workspace(self):
        if (
        self.data_model.is_dirty and 
        
        QtGui.QMessageBox.question(self, _("confirm"),
        _("Abandon Changes and Create a new workbench?"),
        QtGui.QMessageBox.Ok|QtGui.QMessageBox.Cancel,
        QtGui.QMessageBox.Cancel) == QtGui.QMessageBox.Cancel):
            return
        self.data_model.new_workspace()

    def load_workspace(self, filename=False):
        if filename in (False, None, ""):
            filename = QtGui.QFileDialog.getOpenFileName(self,
            "select a workspace", self.workspace_path, 
            "q_auteur xml files (*.xml)")

        if filename != "":
            filename = str(filename).replace(" ", "\ ")
            self.workspace_path = os.path.dirname(filename)
            self.data_model.load(filename)
    
    def save_workspace(self):
        filename = QtGui.QFileDialog.getSaveFileName(self,
        "Save Workspace", self.workspace_path, 
        "q_auteur xml files (*.xml)")

        if filename != "":
            filename = str(filename).replace(" ", "\ ")
            self.workspace_path = os.path.dirname(filename)
            if not filename.endswith(".xml"):
                filename += ".xml"
            result, error = self.data_model.save_file(filename)
            
            if result:
                QtGui.QMessageBox.information(self, _("sucess"), 
                _("workspace saved sucessfully"))
            else:
                QtGui.QMessageBox.warning(self, _("failure"), 
                error)
    
    def update_data(self):
        '''
        the model is calling this something has changed something of note
        '''
        self.action_undo.setEnabled(self.data_model.undo_available)
        self.action_redo.setEnabled(self.data_model.redo_available)
        
        if self.data_model.has_sources:
            self.screen_widget.show_message2()
        else:
            self.screen_widget.show_message1()
        
    def undo(self):
        self.data_model.undo()
        
    def redo(self):
        self.data_model.redo()

def main(filename=None, mplayer_opts=[], mencoder_opts=[]):
    def load_workspace():
        editor.load_workspace(filename)
    app = QtGui.QApplication(sys.argv)
    app.setOrganizationName("q-auteur")
    app.setApplicationName("editor")
    
    editor = MainApp(mplayer_opts, mencoder_opts)
    editor.show()
    if filename:
        QtCore.QTimer.singleShot(0, load_workspace)

    sys.exit(app.exec_())
    
            
if __name__ == "__main__":
    
    import gettext
    gettext.install("q_auteur")
    
    #make the script work from any directory
    def determine_path ():
        """Borrowed from wxglade.py"""
        try:
            root = __file__
            if os.path.islink (root):
                root = os.path.realpath (root)
            retarg = os.path.dirname (os.path.abspath (root))
            return retarg
        except:
            print("I'm sorry, but something is wrong.")
            print("There is no __file__ variable.")
            print("are you running this script from an IDE?")
            sys.exit ()

    wkdir = determine_path()
    sys.path.insert(0, os.path.dirname(wkdir))
    
    main()