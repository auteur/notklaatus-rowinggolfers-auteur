#! /usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
##                                                                           ##
##  Copyright 2010, Neil Wallace <rowinggolfer@googlemail.com>               ##
##                                                                           ##
##  This program is free software: you can redistribute it and/or modify     ##
##  it under the terms of the GNU General Public License as published by     ##
##  the Free Software Foundation, either version 3 of the License, or        ##
##  (at your option) any later version.                                      ##
##                                                                           ##
##  This program is distributed in the hope that it will be useful,          ##
##  but WITHOUT ANY WARRANTY; without even the implied warranty of           ##
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            ##
##  GNU General Public License for more details.                             ##
##                                                                           ##
##  You should have received a copy of the GNU General Public License        ##
##  along with this program.  If not, see <http://www.gnu.org/licenses/>.    ##
##                                                                           ##
###############################################################################

from lib_q_auteur.components import DataModel
import subprocess, sys, time
from PyQt4 import QtGui, QtCore

class RendererDialog(QtGui.QDialog):
    def __init__(self, filename, outfile, mencoder_opts="", parent=None):
        QtGui.QDialog.__init__(self, parent)
                
        self.filename = filename
        self.outfile = outfile
        self.mencoder_opts = mencoder_opts
        
        self.model = DataModel()
        self.model.load(filename)       
        
        self.label =  QtGui.QLabel()
        self.render_label = QtGui.QLabel()
        
        button_box = QtGui.QDialogButtonBox(self)
        button_box.setOrientation(QtCore.Qt.Horizontal)
        
        button = button_box.addButton(button_box.Cancel)
        
        layout = QtGui.QVBoxLayout(self)
        layout.addWidget(self.label)
        layout.addWidget(self.render_label)
        layout.addSpacing(80)
        layout.addWidget(button_box)
        
        button.clicked.connect(self. cancel_process)
        icon = QtGui.QIcon.fromTheme("go-down")
        more_but = QtGui.QPushButton(icon, "&Advanced")
        more_but.setFlat(True)

        more_but.setCheckable(True)
        more_but.setFocusPolicy(QtCore.Qt.NoFocus)
        button_box.addButton(more_but, button_box.HelpRole)
        
        self.setOrientation(QtCore.Qt.Vertical)

        self.cli_text_edit = QtGui.QTextEdit()
        self.setExtension(self.cli_text_edit)

        more_but.clicked.connect(self.showExtension)
        
    def set_label_text(self):
        self.label.setText('''<html><body><div align='center'>
                Rendering your video<br />
                control file <b>%s</b> <br />
                outfile <b>%s</b></div></body></html>'''% (
                self.filename, self.outfile))
    
    def sizeHint(self):
        return QtCore.QSize(200,100)
        
    def cancel_process(self):
        QtGui.QApplication.instance().restoreOverrideCursor()
        try:
            self.process.terminate()
        except AttributeError:
            pass
        self.reject() 
        
    def exec_(self):
        if self.outfile == None:
            self.outfile = QtGui.QFileDialog.getSaveFileName(self, 
            "Please choose an output filename and location",
            "output.ogv")
            if self.outfile == "":
                return False
        self.set_label_text()
        QtCore.QTimer.singleShot(0,self.render)
        return QtGui.QDialog.exec_(self)
        
    def render(self):
        '''
        thanks to klaatu for this mencoder line

        mencoder evildead.ogv -ss 00:13:13 -endpos 00:00:13 -oac pcm -ovc lavc 
        armyofdarkness.ogv -ss 00:66:06 -endpos 00:06:06 -oac pcm -ocv lavc -o 
        ./render/evilcomposite.avi
        '''        
        
        commands = ["mencoder", "-o", self.outfile]
        if self.mencoder_opts != "":
            commands.extend([self.mencoder_opts])
            
        for clip in self.model.clips:
            commands.append(clip.source_file)
            if clip.start != "":
                commands += ["-ss", clip.start_format]
            if clip.end != "":
                commands += ["-endpos", clip.end_format]
                
            commands += ["-oac", "pcm", "-ovc", "lavc"]
        
        command = "mencoder command\n\n"
        for arg in commands:
            command += arg + " "
        
        print("="*80)
        print(command)
        print("="*80)
        self.cli_text_edit.setPlainText(command)
        
        QtGui.QApplication.instance().setOverrideCursor(QtCore.Qt.WaitCursor)
    
        self.process = subprocess.Popen(commands, stdout=subprocess.PIPE)
        bytes = True
        while bytes:
            QtGui.QApplication.instance().processEvents()
            bytes = self.process.stdout.readline()
            r = str(bytes)
            self.render_label.setText(r[:20])
            
        print("finished rendering")
        QtGui.QApplication.instance().restoreOverrideCursor()
        QtGui.QMessageBox.information(self,"message","rendering complete")
        
        self.accept()
    
def main(filename, outfile=None, mencoder_opts=""):
    app = QtGui.QApplication([])
    dl = RendererDialog(filename, outfile, mencoder_opts)
    dl.exec_()
    
def _test():
    import gettext
    gettext.install("auteur")
     
    main("components/test_video/test_project.xml", "test_out.ogv")
    
if __name__ == "__main__":
    _test()
    