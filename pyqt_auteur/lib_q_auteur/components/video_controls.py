#! /usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
##                                                                           ##
##  Copyright 2010, Neil Wallace <rowinggolfer@googlemail.com>               ##
##                                                                           ##
##  This program is free software: you can redistribute it and/or modify     ##
##  it under the terms of the GNU General Public License as published by     ##
##  the Free Software Foundation, either version 3 of the License, or        ##
##  (at your option) any later version.                                      ##
##                                                                           ##
##  This program is distributed in the hope that it will be useful,          ##
##  but WITHOUT ANY WARRANTY; without even the implied warranty of           ##
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            ##
##  GNU General Public License for more details.                             ##
##                                                                           ##
##  You should have received a copy of the GNU General Public License        ##
##  along with this program.  If not, see <http://www.gnu.org/licenses/>.    ##
##                                                                           ##
###############################################################################

 
import os, sys

from PyQt4 import QtCore, QtGui

class VideoControls(QtGui.QWidget):
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)
        
        self.slider = QtGui.QSlider()
        self.slider.setMinimumWidth(120)
        self.slider.setOrientation(QtCore.Qt.Horizontal)
        self.slider.setRange(0,100)
        self.slider.setValue(0)

        self.label = QtGui.QLabel("monitor")
        self.label.setMinimumWidth(120)        
        self.toolbar = QtGui.QToolBar(self)
        self.toolbar.setToolButtonStyle(QtCore.Qt.ToolButtonTextUnderIcon)

        self.insert_player_buttons()
        
        self.layout = QtGui.QGridLayout(self)
        self.layout.setMargin(0)
        self.layout.setSpacing(2)
        self.layout.addWidget(self.slider,0,0)
        self.layout.addWidget(self.label,0,1)
        self.layout.addWidget(self.toolbar,1,0,1,2)
        
        self.setMaximumHeight(80)
        
        #the label is used to pass messages over.
        self.connect(self.label, QtCore.SIGNAL("completed"), self.reset)
        self.connect(self.label, QtCore.SIGNAL("paused"), self.paused)

        
    def sizeHint(self):
        return QtCore.QSize(400,80)    
    
    def minimumSizeHint(self):
        return QtCore.QSize(250,80)
    
    def insert_player_buttons(self):
        
        self.video_controls = []
        
        icon = QtGui.QIcon.fromTheme("media-skip-backward")
        self.action_skipback = QtGui.QAction(icon, _("skip"), self)
        self.toolbar.addAction(self.action_skipback)
        self.video_controls.append(self.action_skipback)
        
        icon = QtGui.QIcon.fromTheme("media-seek-backward")
        self.action_seekback = QtGui.QAction(icon, _("reverse"), self)
        self.toolbar.addAction(self.action_seekback)
        self.video_controls.append(self.action_seekback)
        
        self.play_icon = QtGui.QIcon.fromTheme("media-playback-start")
        self.pause_icon = QtGui.QIcon.fromTheme("media-playback-pause")
        self.play_pause = QtGui.QAction(self.play_icon, _("Play"), self)
        self.toolbar.addAction(self.play_pause)
        self.video_controls.append(self.play_pause)
        
        icon = QtGui.QIcon.fromTheme("media-playback-stop")
        self.action_stop = QtGui.QAction(icon, _("Stop"), self)
        self.toolbar.addAction(self.action_stop)
        self.video_controls.append(self.action_stop)
                
        icon = QtGui.QIcon.fromTheme("media-seek-forward")
        self.action_seekforward = QtGui.QAction(icon, _("forwards"), self)
        self.action_seekforward.setToolTip(_("double speed"))
        self.toolbar.addAction(self.action_seekforward)
        self.video_controls.append(self.action_seekforward)
        
        icon = QtGui.QIcon.fromTheme("media-skip-forward")
        self.action_skipforward = QtGui.QAction(icon, _("skip"), self)
        self.toolbar.addAction(self.action_skipforward)
        self.video_controls.append(self.action_skipforward)
                
        self.toolbar.addSeparator()
        frame = QtGui.QFrame()
        self.speed_label = QtGui.QLabel("%s %.1fx"% (_("Speed"), 1.0))
        self.speed_dial = QtGui.QDial()
        self.speed_dial.setFixedSize(40,40)
        self.speed_dial.setRange(1,10000)
        self.speed_dial.setValue(100)
        self.speed_dial.setNotchesVisible(True)
        self.speed_dial.setNotchTarget(100)

        self.speed_dial.valueChanged.connect(self.speed_set)

        layout = QtGui.QHBoxLayout(frame)
        layout.setMargin(0)
        layout.addWidget(self.speed_label)
        layout.addWidget(self.speed_dial)
        self.toolbar.addWidget(frame)
        self.video_controls.append(frame)
        
        self.toolbar.addSeparator()
        icon = QtGui.QIcon.fromTheme("edit-cut")
        self.action_split = QtGui.QAction(icon, _("Split Clip"), self)
        self.action_split.setToolTip(
            _("Split the clip at the current time position"))  
        self.toolbar.addAction(self.action_split)
        self.video_controls.append(self.action_split)

        self.enable(False)
        
    def reset(self):
        self.paused(True)
        self.speed_dial.setValue(100)
        self.slider.setValue(0)
        
    def paused(self, i):
        '''
        called via a signal in the monitoring thread
        '''
        if i:
            self.play_pause.setIcon(self.play_icon)
            self.play_pause.setText(_("Play"))
        else:
            self.play_pause.setIcon(self.pause_icon)
            self.play_pause.setText(_("Pause"))
        
        
    def enable(self, enable=True):
        for action in self.video_controls:
            action.setEnabled(enable)
            
    def speed_set(self, i):
        i = i/100
        self.speed_label.setText("Speed %.02f"% i)
        self.emit(QtCore.SIGNAL("speed_set"), i)
            
    def disable(self):
        self.enable(False)
        
class DockedVideoControls(QtGui.QDockWidget):
    '''
    A dockWidget wrapper for the video toolbar
    '''
    def __init__(self, controls, parent=None):
        
        assert type(controls) == VideoControls 
        
        QtGui.QDockWidget.__init__(self, parent)
        
        self.setWindowTitle(_("Player Controls"))
    
        self.setWidget(controls)
       
    
def _test_controls():
    app = QtGui.QApplication([])
    
    import gettext
    gettext.install("auteur")
    
    vc = VideoControls()        
    vc.enable()
    
    obj = DockedVideoControls(vc)
    obj.show()

    app.exec_()    

if __name__ == "__main__":
    _test_controls()
