#! /usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
##                                                                           ##
##  Copyright 2010, Neil Wallace <rowinggolfer@googlemail.com>               ##
##                                                                           ##
##  This program is free software: you can redistribute it and/or modify     ##
##  it under the terms of the GNU General Public License as published by     ##
##  the Free Software Foundation, either version 3 of the License, or        ##
##  (at your option) any later version.                                      ##
##                                                                           ##
##  This program is distributed in the hope that it will be useful,          ##
##  but WITHOUT ANY WARRANTY; without even the implied warranty of           ##
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            ##
##  GNU General Public License for more details.                             ##
##                                                                           ##
##  You should have received a copy of the GNU General Public License        ##
##  along with this program.  If not, see <http://www.gnu.org/licenses/>.    ##
##                                                                           ##
###############################################################################

import re
from PyQt4 import QtCore, QtGui

class LogWidget(QtGui.QTextEdit):
    def __init__(self, parent=None):
        QtGui.QTextEdit.__init__(self, parent)
        
        self._data_received = False
        
        self.setText(
        "This editable widget details your decisions.\n\n"
        "You can also edit this manually")

        self.ignore_begin_point = True
        
        self.model = None
        
    def set_model(self, model):
        '''
        attach the view to the data_model
        '''
        self.model = model
            
    def clear_if_necessary(self):
        if not self._data_received:
            self.clear()
            self._data_received = True
                 
    def receive_data(self, time_point):
        self.clear_if_necessary()
        
        data = "split %d : %s secs"% (self.split_no, time_point)
        self.append(data)
        self.split_no += 1
    
    @property
    def split_points(self):
        '''
        convert the text into a string list of split points
        will be in the form ['19.1', '20.4'] 
        '''
        if not self._data_received:
            return []
        text = self.document().toPlainText()
        data = str(text.toAscii())
        
        #do some regex to remove my user friendly(?) stuff
        times = re.findall("(\d+\.\d+) secs", data)
         
        return times
    
    def add_source_file(self, filename):
        '''
        adds an entire video file to the timeline
        '''
        self.clear_if_necessary()
        
        self.append("Source File Added")
        self.append(" - "+filename)
        
    def update_data(self):
        '''
        this is called from the data_model itself, when something happens.
        '''
        self.setText(self.model.log_widget_text)
        
        
class DockedLogWidget(QtGui.QDockWidget):
    '''
    A dockWidget wrapper for the video toolbar
    '''
    def __init__(self, widget, parent=None):
        
        assert type(widget) == LogWidget 
        
        QtGui.QDockWidget.__init__(self, parent)
        
        self.setWindowTitle(_("Log Widget"))
    
        self.setWidget(widget)
       

def _test_edl():
    app = QtGui.QApplication([])
    
    import gettext
    gettext.install("auteur")
    
    obj = LogWidget()        
    
    wrap = DockedLogWidget(obj)
    wrap.show()

    app.exec_()    

if __name__ == "__main__":
    _test_edl()
