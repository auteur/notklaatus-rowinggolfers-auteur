#! /usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
##                                                                           ##
##  Copyright 2010, Neil Wallace <rowinggolfer@googlemail.com>               ##
##                                                                           ##
##  This program is free software: you can redistribute it and/or modify     ##
##  it under the terms of the GNU General Public License as published by     ##
##  the Free Software Foundation, either version 3 of the License, or        ##
##  (at your option) any later version.                                      ##
##                                                                           ##
##  This program is distributed in the hope that it will be useful,          ##
##  but WITHOUT ANY WARRANTY; without even the implied warranty of           ##
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            ##
##  GNU General Public License for more details.                             ##
##                                                                           ##
##  You should have received a copy of the GNU General Public License        ##
##  along with this program.  If not, see <http://www.gnu.org/licenses/>.    ##
##                                                                           ##
###############################################################################

import os, subprocess
from PyQt4 import QtCore, QtGui

if __name__ == "__main__":
    import sys
    sys.path.insert(0, "../../")
    
from lib_q_auteur.components.clip_properties_dialog import ClipPropertiesDialog

class ClickableLabel(QtGui.QLabel):
    def __init__(self, parent = None):
        QtGui.QLabel.__init__(self, parent)
        self.setMouseTracking(True)
        self.mouse_over = False
        self.is_selected = False
        self.clip = None
        
    def sizeHint(self):
        return QtCore.QSize(80,80)
        
    def mouseMoveEvent(self, event):
        if self.is_selected:
            self.mouse_over = False
        else:
            self.mouse_over = True
            self.update()
        
    def mousePressEvent(self, event):
        self.is_selected = True
        self.mouse_over = False
        if event.button() == 2: # right clicked
            self.emit(QtCore.SIGNAL("right clicked"), event.globalPos())
        else:
            self.emit(QtCore.SIGNAL("clicked"))
        self.update()
    
    def select(self):
        self.is_selected = True
        self.emit(QtCore.SIGNAL("clicked"))
        self.update()
        
    def leaveEvent(self, event):
        self.mouse_over = False
        self.update()
            
    def sizeHint(self):
        return QtCore.QSize(80, 80)
        
    def paintEvent(self, event):
        painter = QtGui.QPainter(self)
        painter.drawPixmap(self.rect(), self.pixmap())
        option = QtGui.QTextOption()
        option.setAlignment(QtCore.Qt.AlignCenter)
        painter.setPen(QtGui.QColor("red"))
        if self.clip.is_partial:
            message = "- %s -"% _("clip")
        else:
            message = "- %s -"% _("source")
        #painter.rotate(90)
        painter.drawText(QtCore.QRectF(self.rect()), message, option)
            
        painter.fillRect(QtCore.QRect(0,0,8,self.height()), 
            QtGui.QColor("black"))
            
        painter.fillRect(QtCore.QRect(self.width()-8,0,8,self.height()), 
            QtGui.QColor("black"))
            
        y = 2
        while y < self.height():
            
            painter.fillRect(QtCore.QRect(2,y,5,5), QtGui.QColor("white"))
            painter.fillRect(QtCore.QRect(self.width()-6,y,5,5), 
                QtGui.QColor("white"))
            
            y+=10

        if self.is_selected:
            painter.save()
            pen = QtGui.QPen(QtGui.QColor("blue"), 2)
            painter.setPen(pen)
            painter.drawRect(self.rect().adjusted(1,1,-1,-1)) 
            painter.restore()
                       
        if self.mouse_over:
            painter.save()
            pen = QtGui.QPen(QtGui.QColor("aqua"), 2)
            painter.setPen(pen)
            painter.drawRect(self.rect().adjusted(1,1,-1,-1))
            painter.restore()
        

class ClipsWidget(QtGui.QWidget):
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)
        
        self.mencoder_opts = ""
        self.clip_layout = None
        
        label = QtGui.QLabel(_("no clips added yet"))
                
        self.layout = QtGui.QGridLayout(self)
        self.layout.addWidget(label)
        
        self.model = None
        
    def set_model(self, model):
        '''
        attach the view to the data_model
        '''
        self.model = model
    
    def set_mencoder_opts(self, opts):
        self.mencoder_opts = opts
    
    def add_clip_layout(self):
        frame = QtGui.QFrame()
        self.clip_layout = QtGui.QHBoxLayout(frame)
        self.clip_layout.addStretch(0)

        scroll_area = QtGui.QScrollArea(self)
        scroll_area.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        scroll_area.setWidgetResizable(True)
        scroll_area.setWidget(frame)

        icon = QtGui.QIcon.fromTheme("media-playback-start")
        view_button = QtGui.QPushButton(_("Preview"))
        view_button.setIcon(icon)
        
        render_button = QtGui.QPushButton(_("Render"))
        icon = QtGui.QIcon.fromTheme("media-record")
        render_button.setIcon(icon)
        self.length_label = QtGui.QLabel()
        
        self.layout.addWidget(scroll_area,0,0,3,1)
        self.layout.addWidget(self.length_label,0,1)
        self.layout.addWidget(view_button,1,1)
        self.layout.addWidget(render_button,2,1)
        
        view_button.clicked.connect(self.preview)
        render_button.clicked.connect(self.render)
    
    @property
    def clips(self):
        return self.model.clips
    
    def preview(self):
        if self.clips == []:
            QtGui.QMessageBox.information(self, "whoops", _("nothing to play"))
            return
        try:
            subprocess.Popen(["q_auteur", "--play", self.model.tempfile]).pid
        except OSError:
            message = ("whoops.. misunderstood system call\n" 
                    "is q_auteur on your sys path?\n"
                    "if you are running a non-installed version\n"
                    "please put a symlink in /usr/local/bin")
            QtGui.QMessageBox.warning(self, "error", message)
    
    def render(self):
        '''
        render the clips
        '''        
        
        if self.clips == []:
            QtGui.QMessageBox.information(self, "whoops", 
            _("nothing to render"))
            return
        
        outfile = QtGui.QFileDialog.getSaveFileName(self, 
        "Please choose a filename and location",
        "output.ogv")
        if outfile == "":
            return
        
        commands = ["q_auteur", "--render", self.model.tempfile, "-o", outfile]
        
        if self.mencoder_opts != "":
            commands.extend(["--mencoder", self.mencoder_opts]) 
        
        print(commands)
        try:
            subprocess.Popen(commands).pid
        except OSError:
            message = ("whoops.. misunderstood system call\n" 
                    "is q_auteur on your sys path?\n"
                    "if you are running a non-installed version\n"
                    "please put a symlink in /usr/local/bin")
            QtGui.QMessageBox.warning(self, "error", message)
        
    
    def add_clip(self, clip):
        label = ClickableLabel()
        label.clip = clip
        label.setToolTip(clip.details)
        label.setPixmap(clip.thumbnail)
        self.clip_layout.insertWidget(self.clip_layout.count()-1,label)
    
        self.connect(label, QtCore.SIGNAL("clicked"), self.clip_clicked)
        self.connect(label, QtCore.SIGNAL("right clicked"), 
            self.clip_right_clicked)
        
    def deselect_cliplabels(self, label):
        '''
        deselects all but the named label
        '''    
        for i in range(self.clip_layout.count()-1):
            widg = self.clip_layout.itemAt(i).widget()
            if widg != label:
                widg.is_selected = False
                widg.update()
    
    def select_clip_from_id(self, id):
        '''
        selects the clip with given id
        '''    
        for i in range(self.clip_layout.count()-1):
            widg = self.clip_layout.itemAt(i).widget()
            if widg.clip.id == id:
                widg.select()
                break
                
    def clip_clicked(self):
        label = self.sender()
        self.deselect_cliplabels(label)
        self.emit(QtCore.SIGNAL("clip selected"), self.selected_clip)
    
    def clip_right_clicked(self, pos):
        ## there was an issue here.. 
        label = self.sender()
        self.deselect_cliplabels(label)
        self.emit(QtCore.SIGNAL("clip selected"), self.selected_clip)
    
        options = [
            _("Load clip into editor"), 
            _("Show Source Video Properties"),
            _("Delete Clip")
            ]
        menu = QtGui.QMenu(self)
        for option in options:
            menu.addAction(option)
        result = menu.exec_(pos)
        if not result:
            return
        if result.text() == options[0]:
            self.clip_clicked()
        elif result.text() == options[1]:
            dl = ClipPropertiesDialog(label.clip, self)
            dl.exec_()
        elif result.text() == options[2]:
            if QtGui.QMessageBox.question(self, _("confirm"),
            _("delete clip?"),
            QtGui.QMessageBox.Ok|QtGui.QMessageBox.Cancel,
            QtGui.QMessageBox.Ok) == QtGui.QMessageBox.Ok:
                self.emit(QtCore.SIGNAL("delete clip"))
                
    @property
    def selected_clip(self):            
        for i in range(self.clip_layout.count()-1):
            widg = self.clip_layout.itemAt(i).widget()
            if widg.is_selected:
                return widg.clip
            
    def update_data(self):
        if self.clip_layout == None:
            self.add_clip_layout()
        else:
            while self.clip_layout.count()>1:
                item = self.clip_layout.takeAt(0)
                item.widget().deleteLater()
        
        total_length = 0
        for clip in self.clips:
            total_length += clip.length
            self.add_clip(clip)    
            
        self.length_label.setText("Combined Length %d seconds"% total_length)
        
class DockedClipsWidget(QtGui.QDockWidget):
    '''
    A dockWidget wrapper for the ClipsWidget
    '''
    def __init__(self, widget, parent=None):
        
        assert type(widget) == ClipsWidget 
        
        QtGui.QDockWidget.__init__(self, parent)
        
        self.setWindowTitle(_("Clips Widget"))
    
        self.setWidget(widget)
       

def _test_code():
    app = QtGui.QApplication([])
    
    import gettext
    gettext.install("auteur")
    
    obj = ClipsWidget() 
    obj.set_mencoder_opts("-vf scale=640:480")
    
    import sys
    sys.path.insert(0, "../../")
    from lib_q_auteur.components.data_model import DataModel
    
    model = DataModel() 
    model.add_source_file("test_video/greetings_and_salutations.ogv")
    
    obj.set_model(model)
    obj.update_data()
    
    wrap = DockedClipsWidget(obj)
    wrap.show()

    app.exec_()    

if __name__ == "__main__":
    _test_code()
