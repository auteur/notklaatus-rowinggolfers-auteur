
###############################################################################
##                                                                           ##
##  Copyright 2010, Neil Wallace <rowinggolfer@googlemail.com>               ##
##                                                                           ##
##  This program is free software: you can redistribute it and/or modify     ##
##  it under the terms of the GNU General Public License as published by     ##
##  the Free Software Foundation, either version 3 of the License, or        ##
##  (at your option) any later version.                                      ##
##                                                                           ##
##  This program is distributed in the hope that it will be useful,          ##
##  but WITHOUT ANY WARRANTY; without even the implied warranty of           ##
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            ##
##  GNU General Public License for more details.                             ##
##                                                                           ##
##  You should have received a copy of the GNU General Public License        ##
##  along with this program.  If not, see <http://www.gnu.org/licenses/>.    ##
##                                                                           ##
###############################################################################

'''
my_core is a re-implementation of code from the python-mplayer code

which is 

Copyright (C) 2010  Darwin M. Bautista <djclue917@gmail.com>

why? The -quiet flag is unwanted for my editor.

I need to leverage more of mplayer's output, in a purely pyqt manner
'''

import os, re, subprocess, sys
 
from PyQt4 import QtGui, QtCore

try:
    raw_input
    PYTHON3 = False
except NameError:
    PYTHON3 = True 


class MonitoringThread(QtCore.QThread):

    def __init__(self, label, process, parent=None):
        QtCore.QThread.__init__(self, parent)
        self.process = process
        
        self.label = label
        self.progress_indicator = None
        self.timer = None
        self.is_paused = True
        
        self.get_output = self.get_output3 if PYTHON3 else self.get_output2
        
    def set_progress_indicator(self, pb):
        try:
            pb.setValue
        except AttributeError:
            raise AssertionError(
            "%s does not have a setValue Function"% pb +
            "your progress indicator will not work")
            
        self.progress_indicator = pb
        self.timer = QtCore.QTimer(self)
        self.timer.setInterval(1000)
        self.timer.timeout.connect(self.check_progress)
        self.timer.start()
    
    def check_progress(self):
        try:
            self.process.stdin.write("pausing_keep_force get_percent_pos\n")
            self.process.stdin.flush()
            self.label.emit(QtCore.SIGNAL("paused"), self.is_paused)
        except IOError as e:
            #print ("HANDLED IOError", e)
            #print (self.process.pid)
            self.label.emit(QtCore.SIGNAL("completed"))
            self.timer.stop()
            self.quit() 
            
    def get_output3(self):
        '''
        python3 version of stdout parsing
        '''
        return self.process.stdout.readline().strip("\r")
    
    def get_output2(self):
        '''
        python2 version of stdout parsing
        '''
        return self.process.stdout.readline().decode("ascii").strip("\r")
    
    def run(self):
        '''
        we watch out for information returned on standard out,
        either as a result of a video playing, or by queries sent by the user
        
        as an example, when the user sends "get_time_position" into stdout
        ANS_TIME_POSITION=12.7 is returned.
        
        we watch for this and emit a signal via the supplied Qlabel
        '''
        out =  self.get_output()
        while out:
            if out.startswith("  =====  PAUSE  ====="):
                self.is_paused = True
            
            elif out.startswith("ANS_PERCENT_POSITION="):
                if self.progress_indicator:
                    m = re.match("ANS_PERCENT_POSITION=(\d+)", out)
                    val = int(m.groups()[0])
                    self.progress_indicator.setValue(val)
            
            elif out.startswith("ANS_TIME_POSITION="):
                m = re.match("ANS_TIME_POSITION=(\d+\.\d+)", out)
                self.label.emit(QtCore.SIGNAL("split position"),
                    float(m.groups()[0]))
            
            else:
                self.is_paused = False
                self.label.setText(out)
            
            #debug
            #print ("'%s'"% out)
            
            out =  self.get_output()
        
        print("STDOUT FINISHED")
        if not self.progress_indicator:
            self.quit() # destroy this thread
        else:
            self.progress_indicator.setValue(0)
            
class Player(object):
    """
    Player(args=(), stdout=PIPE, stderr=None)
    """
    devnull = open("/dev/null", "w")
    
    def __init__(self, args=[], stdout=subprocess.PIPE, stderr=devnull):
        
        self.args = [
            'mplayer',
            '-ni',
            '-slave',
            '-idle', 
            '-input',
            'nodefault-bindings',
            '-noconfig', 
            'all', 
            '-osdlevel', '3',
            '-loop', '0',
            ]
            
        
        self.args.extend(args)
        self._proc = None
        
        self.stdout = stdout
        self.stderr = stderr
        self.label = None
        self.progress_indicator = None
        self.controls = None
        self._playlist = []
        
    def __del__(self):
        self.quit()

    def __repr__(self):
        if self.is_alive():
            status = 'with pid = %d' % (self._proc.pid)
        else:
            status = 'not running'
        return '<%s.%s %s>' % (__name__, self.__class__.__name__, status)

    @property
    def playlist(self):
        return self._playlist
    
    def start_monitor_timer(self):
        '''
        start the QTimer which is pumping into standard in
        '''
        try:
            self.monitor.timer.start()
        except AttributeError:
            pass
            
    def stop_monitor_timer(self):
        '''
        start the QTimer which is pumping into standard in
        '''
        try:
            self.monitor.timer.stop()
        except AttributeError:
            pass
        
    def set_playlist(self, playlist):
        self._playlist = playlist
    
    def add_to_playlist(self, filename):
        filename = str(filename).replace(" ", "\ ")
        if self.is_alive():
            self._command("loadfile %s"% filename)
            self._playlist = [filename]
        else:
            self._playlist.append(filename) 
           
    def start(self, winId, additional_args=[]):
        '''
        Start the MPlayer process.
        Returns None if MPlayer is already running.
        '''
        if self.playlist == []:
            print("no playlist yet!")
            return
        if not self.is_alive():
            mplayer_args = self.args[:]
            mplayer_args.extend(["-wid", str(winId)] )
            mplayer_args.extend(additional_args)
            mplayer_args.extend(self.playlist)        
            
            print(mplayer_args)

            self._proc = subprocess.Popen(mplayer_args, 
                stdin=subprocess.PIPE,
                stdout=subprocess.PIPE, 
                stderr=self.stderr,
                universal_newlines=True)

            self._stdout = self._proc.stdout
            self._stderr = self._proc.stderr
        
        if self.label:
            self.monitor = MonitoringThread(self.label, self._proc)
            self.monitor.start()
        if self.progress_indicator:
            self.monitor.set_progress_indicator(self.progress_indicator)
                        
    def set_label(self, label):
        assert type(label) == QtGui.QLabel, "label must be a QLabel"
        self.label = label
        
    def set_progress_indicator(self, progress_bar):
        assert progress_bar.setValue, (
            'ERROR mplayer_core set_progress_indicator' +
            '- widget must have a setValue function')
        
        self.progress_indicator = progress_bar
        
    def quit(self, retcode=0):
        """Stop the MPlayer process.

        Returns the exit status of MPlayer or None if not running.
        """
        if self.is_alive():
            self._stdout = None
            self._stderr = None
            self._proc.stdin.write('quit %d\n' % (retcode, ))
            self._proc.stdin.flush()
            return self._proc.wait()

    def is_alive(self):
        """Check if MPlayer process is alive.

        Returns True if alive, else, returns False.
        """
        if self._proc is None:
            return False
        else:
            return (self._proc.poll() is None)
        
    def _command(self, command):
        '''
        Send a command to MPlayer.
        Valid MPlayer commands are documented in:
        http://www.mplayerhq.hu/DOCS/tech/slave.txt
        '''
        if command.startswith('quit'):
            self.quit()
        if self.is_alive() and command:
            self._proc.stdin.write(command+"\n")
            self._proc.stdin.flush()


class _TestDialog(QtGui.QDialog):
    def __init__(self):
        QtGui.QDialog.__init__(self, None)

        self.video_path = ""
        self.playlist = sys.argv[1:]

        self.screen = QtGui.QLabel("no video loaded")
        self.screen.setAlignment(QtCore.Qt.AlignCenter)
        self.screen.setStyleSheet("background:black;color:white")

        self.label = QtGui.QLabel("details")
        self.label.setMaximumHeight(self.fontMetrics().height())
        
        icon = QtGui.QIcon.fromTheme("document-open")
        open_but = QtGui.QPushButton(icon, "", self)
        open_but.setFocusPolicy(QtCore.Qt.NoFocus)
        
        
        layout = QtGui.QGridLayout(self)
        layout.setSpacing(0)
        layout.addWidget(self.screen,0,0,1,2)        
        layout.addWidget(open_but,1,0)
        layout.addWidget(self.label,1,1)        
        
        self.player = Player()
            
        self.player.set_label(self.label)
        
        self.rejected.connect(self.quit)
        
        open_but.clicked.connect(self.load_video)
        
        if self.playlist != []:
            QtCore.QTimer.singleShot(0, self.start_player)
        
    def sizeHint(self):
        return QtCore.QSize(300,300)

    def quit(self):
        self.player.quit()
        
    def clear(self):
        self.player.quit()
        self.screen.show()

    def start_player(self):
        self.player.set_playlist(self.playlist)
        self.player.start(self.screen.winId())
                
    def load_video(self):
        filename = QtGui.QFileDialog.getOpenFileName(self,
        "select video sources", self.video_path, "video files (*.*)")
        if filename:
            pynames = [str(filename)]
            self.video_path = os.path.dirname(str(filename))
            if self.player.is_alive():
                self.player.quit()
            self.playlist = pynames
            self.start_player()
    
    def exec_(self):
        self.show()
        QtGui.QDialog.exec_(self)

def _test_main():
    dl = _TestDialog()
    dl.exec_()

if __name__ == '__main__':

    app = QtGui.QApplication([])
    _test_main()
    