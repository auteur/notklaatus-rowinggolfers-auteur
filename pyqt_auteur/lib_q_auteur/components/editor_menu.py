#! /usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
##                                                                           ##
##  Copyright 2010, Neil Wallace <rowinggolfer@googlemail.com>               ##
##                                                                           ##
##  This program is free software: you can redistribute it and/or modify     ##
##  it under the terms of the GNU General Public License as published by     ##
##  the Free Software Foundation, either version 3 of the License, or        ##
##  (at your option) any later version.                                      ##
##                                                                           ##
##  This program is distributed in the hope that it will be useful,          ##
##  but WITHOUT ANY WARRANTY; without even the implied warranty of           ##
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            ##
##  GNU General Public License for more details.                             ##
##                                                                           ##
##  You should have received a copy of the GNU General Public License        ##
##  along with this program.  If not, see <http://www.gnu.org/licenses/>.    ##
##                                                                           ##
###############################################################################

from PyQt4 import QtCore, QtGui

class EditorMenu(QtCore.QObject):
    def __init__(self):
        '''
        parent should be a mainwindow
        '''
        self.menubar = self.menuBar()
        
        self.tool_bar = QtGui.QToolBar()
        self.tool_bar.setObjectName("editor-toolbar")
        
        self.init_actions()
        self.init_menu()
        self.init_toolbar()
    
    def init_actions(self):
        
        ####          file menu                                            ####

        icon = QtGui.QIcon.fromTheme("application-exit")
        self.action_quit = QtGui.QAction(icon, _("Quit"), self)

        icon = QtGui.QIcon.fromTheme("document-new")
        self.action_new_workspace = QtGui.QAction(icon, _("New WorkBench"), 
            self)
        
        icon = QtGui.QIcon.fromTheme("document-open")
        self.action_load_workspace = QtGui.QAction(icon, _("Load WorkBench"), 
            self)

        icon = QtGui.QIcon.fromTheme("document-save")
        self.action_save_workspace = QtGui.QAction(icon, _("Save WorkBench"), 
            self)

        icon = QtGui.QIcon(":folder_video.png")
        self.action_get_sources = QtGui.QAction(icon, _("Get Source Video(s)"), 
            self)
        
        ####        edit menu                                              ####
        icon = QtGui.QIcon.fromTheme("edit-undo")
        self.action_undo = QtGui.QAction(icon, _("Undo Last Edit"), self)
        self.action_undo.setEnabled(False)
        
        icon = QtGui.QIcon.fromTheme("edit-redo")
        self.action_redo = QtGui.QAction(icon, _("Redo"), self)
        self.action_redo.setEnabled(False)
        


        ####         view menu                                             ####

        self.action_docked = QtGui.QAction(_("Toolbars"), self)
        self.action_docked.setCheckable(True)
        self.action_docked.setChecked(True)
        self.action_docked = QtGui.QAction(_("Docked Windows"), self)
        self.action_docked.setCheckable(True)
        self.action_multi = QtGui.QAction(_("Multiple Top Level Windows"),self)
        self.action_multi.setCheckable(True)
        
        self.action_show_video_controls = QtGui.QAction(self)
        self.action_show_video_controls.setText(_("Controls"))
        self.action_show_video_controls.setCheckable(True)
        self.action_show_video_controls.setChecked(True)

        self.action_show_screen_widget = QtGui.QAction(self)
        self.action_show_screen_widget.setText(_("Screen"))
        self.action_show_screen_widget.setCheckable(True)
        self.action_show_screen_widget.setChecked(True)
        
        self.action_show_split_source_widget = QtGui.QAction(self)
        self.action_show_split_source_widget.setText(_("Split Sources Widget"))
        self.action_show_split_source_widget.setCheckable(True)
        self.action_show_split_source_widget.setChecked(True)
        
        icon = QtGui.QIcon.fromTheme("view-fullscreen")
        self.action_fullscreen = QtGui.QAction(icon,
            "%s %s"% (_("FullScreen Mode"), "(F11)"), self)
        self.action_fullscreen.setCheckable(True)
        self.action_fullscreen.setShortcut("f11")

        ####         about menu                                            ####

        icon = QtGui.QIcon.fromTheme("help-about")
        self.action_about = QtGui.QAction(icon, _("About"), self)

        self.action_about_qt = QtGui.QAction(icon, _("About Qt"), self)

        self.action_license = QtGui.QAction(icon, _("License"), self)

        icon = QtGui.QIcon.fromTheme("help", QtGui.QIcon("icons/help.png"))
        self.action_help = QtGui.QAction(icon, _("Help"), self)

    def init_menu(self):
        ## menu
        self.menu_file = QtGui.QMenu(_("&File"), self)
        self.menubar.addMenu(self.menu_file)
        self.menu_file.addAction(self.action_new_workspace) 
        self.menu_file.addAction(self.action_load_workspace)
        self.menu_file.addAction(self.action_save_workspace)
        self.menu_file.addSeparator()
        self.menu_file.addAction(self.action_get_sources)
        self.menu_file.addSeparator()
        self.menu_file.addAction(self.action_quit)

        ## edit
        self.menu_edit = QtGui.QMenu(_("&Edit"), self)
        self.menubar.addMenu(self.menu_edit)

        self.menu_edit.addAction(self.action_undo)
        self.menu_edit.addAction(self.action_redo)

        ## view
        self.menu_view = QtGui.QMenu(_("&View"), self)
        self.menubar.addMenu(self.menu_view)

        menu_ui = QtGui.QMenu(_("User Interface Mode"), self)
        menu_ui.addAction(self.action_docked)
        menu_ui.addAction(self.action_multi)
        self.menu_view.addMenu(menu_ui)
        
        self.menu_view.addSeparator()

        menu_view_widgets = QtGui.QMenu(_("Show Widgets"), self)
        menu_view_widgets.addAction(self.action_show_video_controls)
        menu_view_widgets.addAction(self.action_show_screen_widget)
        menu_view_widgets.addAction(self.action_show_split_source_widget)
        
        self.menu_view.addMenu(menu_view_widgets)
        
        self.menu_view.addSeparator()
        
        self.menu_view.addAction(self.action_fullscreen)

        ## help
        self.menu_help = QtGui.QMenu(_("&Help"), self)
        self.menubar.addMenu(self.menu_help)
        
        self.menu_help.addAction(self.action_about)
        self.menu_help.addAction(self.action_license)
        self.menu_help.addAction(self.action_about_qt)
        self.menu_help.addSeparator()
        self.menu_help.addAction(self.action_help)
    
    def init_toolbar(self):
        self.tool_bar.addAction(self.action_new_workspace) 
        self.tool_bar.addAction(self.action_load_workspace)
        self.tool_bar.addAction(self.action_save_workspace)
        self.tool_bar.addSeparator()
        self.tool_bar.addAction(self.action_get_sources)
        self.tool_bar.addSeparator()
        self.tool_bar.addAction(self.action_undo)
        self.tool_bar.addAction(self.action_redo)

class _TestClass(QtGui.QMainWindow, EditorMenu):
    def __init__(self, parent=None):
        QtGui.QMainWindow.__init__(self, parent)
        EditorMenu.__init__(self)
    
        self.addToolBar(self.tool_bar)
    
    def sizeHint(self):
        return QtCore.QSize(400,200)
    
def _test_menu():
    app = QtGui.QApplication([])
    
    import gettext
    gettext.install("auteur")
    
    mw = _TestClass()
   
    mw.show()
    app.exec_()    

if __name__ == "__main__":
    import sys
    sys.path.insert(0, "../../")
    from lib_q_auteur.components.images import q_resources
    _test_menu()
