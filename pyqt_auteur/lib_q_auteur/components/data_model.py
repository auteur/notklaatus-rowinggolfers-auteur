#! /usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
##                                                                           ##
##  Copyright 2010, Neil Wallace <rowinggolfer@googlemail.com>               ##
##                                                                           ##
##  This program is free software: you can redistribute it and/or modify     ##
##  it under the terms of the GNU General Public License as published by     ##
##  the Free Software Foundation, either version 3 of the License, or        ##
##  (at your option) any later version.                                      ##
##                                                                           ##
##  This program is distributed in the hope that it will be useful,          ##
##  but WITHOUT ANY WARRANTY; without even the implied warranty of           ##
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            ##
##  GNU General Public License for more details.                             ##
##                                                                           ##
##  You should have received a copy of the GNU General Public License        ##
##  along with this program.  If not, see <http://www.gnu.org/licenses/>.    ##
##                                                                           ##
###############################################################################

import os, re, subprocess, tempfile

from PyQt4 import QtCore, QtGui
from xml.dom import minidom

class Clip(object):
    '''
    a custom data type to describe a clip
    '''
    def __init__(self):
        self.source_file = ""
        self._id = ""
        self._properties = None
        
        self._start = ""
        self._end = ""
        self._screenshot_pos = None
        
        self._image = None
                
    @property
    def id(self):
        '''
        A UNIQUE 4 digit code for the clip
        '''
        return self._id
    
    @property
    def screenshot_pos(self):
        if self._screenshot_pos == None:
            try:
                self._screenshot_pos = "%.1f"% float(self.start)
            except ValueError:
                self._screenshot_pos = "0.0"
        return self._screenshot_pos
    
    @property
    def start(self):
        '''
        the start position of the clip within the source video file
        default is ""   (ie. the start)
        '''
        return self._start
    
    @property
    def end(self):
        '''
        the end position of the clip within the source video file
        default is ""   (ie. the end)
        '''
        return self._end
    
    @property
    def start_format(self):
        '''
        the start in format "00:00:00.0"
        '''
        if self.start == "":
            return
        start = float(self.start)
        assert start >= 0, (
            "FATAL ERROR: clip id (%s)has a negative start time"% self.id)
        hours = start//3600
        start = start - hours*3600
        mins = start//60
        secs = start-mins*60
        tenths = (secs - int(secs))*10
        return "%02d:%02d:%02d.%d"% (hours, mins, int(secs), tenths)
        
    @property
    def end_format(self):
        '''
        the end in format "00:00:00.0" (for --endpos)

        NOTE - in mplayer/mencoder --endpos is dependendent on the value of 
        --ss
        '''
        if self.end == "":
            return
        end = float(self.end)
        if self.start != "":
            end = end - float(self.start)
        assert end > 0, (
            "FATAL ERROR: clip id (%s) is of negative length"% self.id)
        hours = end//3600
        end = end - hours*3600
        mins = end//60
        secs = end-mins*60
        tenths = (secs - int(secs))*10
        return "%02d:%02d:%02d.%d"% (hours, mins, int(secs), tenths)
        
    @property
    def details(self):        
        return "Clip ID %s<br />source '%s'<br />Start %s<br />End %s"% (
                self.id,
                self.source_file, 
                self.start,
                self.end)
                
    @property
    def edl_file(self):
        '''
        creates an edl_file for the clip, and returns it's path
        an EDL file is an mplayer specific 
        "Editing Data List"
        '''
        edl = ""
        if self.start != "":
            edl += "0 %s 0\n"% self.start
        if self.end != "":
            edl += "%s %s 0\n"% (self.end, self.length)
        temp = tempfile.NamedTemporaryFile(delete=False)
        temp.file.write(edl)
        temp.close()
        print("saving edl", edl)
        return temp.name
    
    @property
    def is_partial(self):
        '''
        does the clip equate to the whole of the source?
        ''' 
        return not (self.start == "" and self.end == "")
    
    @property
    def thumbnail(self):
        return self.image.scaled(80,80)

    @property
    def image(self):
        if self._image == None:
            try:
                self._image = KNOWN_IMAGES[self.id]
                return self._image
            except KeyError:
                pass
            try:
                temp = tempfile.gettempdir()
                src = os.path.join(temp, "00000001.jpg")
                if os.path.exists(src):
                    os.remove(src)
                #use mplayer to grab a screenshot (ss)
                p = subprocess.Popen(["mplayer", "-really-quiet", "-ss", 
                    self.screenshot_pos, self.source_file,
                    "-frames", "1", "-nosound", "-vo", 
                    "jpeg:outdir=%s"% temp])
                p.wait()
                pixmap = QtGui.QPixmap(src)
            except Exception as e:
                print("unable to get image", e)
                pixmap = QtGui.QPixmap()
                
            if pixmap.isNull():
                pixmap = QtGui.QPixmap(":unknown.png")
            
            KNOWN_IMAGES[self.id] = pixmap
            self._image = pixmap
            
        return self._image

    @property
    def length(self):
        if self.start != "":
            start = float(self.start)
        else:
            start = 0
        if self.end != "":
            return float(self.end) - start
        
        source_len = float(self.properties.get("ID_LENGTH", 0))

        return source_len - start
        
            
    @property
    def properties(self):
        if self._properties == None:
            try:
                self._properties = KNOWN_PROPERTIES[self.id]
                return self._properties
            except KeyError:
                pass
            self._properties = {}
            try:
                #use mplayer to get clip properties
                p = subprocess.Popen(["mplayer", "-vo", "null", "-ao", "null",
                "-frames", "0", "-identify", self.source_file], 
                stdout=subprocess.PIPE)
                
                result = p.communicate()[0].decode("ascii")

                for val in (
                    "ID_FILENAME",
                    "ID_DEMUXER",
                    "ID_LENGTH", 
                    "ID_VIDEO_FORMAT", 
                    "ID_VIDEO_BITRATE",
                    "ID_VIEDO_WIDTH",
                    "ID_VIDEO_HEIGHT",
                    "ID_VIDEO_FPS",
                    "ID_VIDEO_ASPECT",
                    "ID_AUDIO_FORMAT",
                    "ID_AUDIO_BITRATE",
                    "ID_AUDIO_RATE",
                    "ID_AUDIO_NCH",
                    "ID_SEEKABLE",
                    "ID_CHAPTERS",                   
                ):
                
                    match=re.search("%s=(.*)\n"% val, result)
                    if match:
                        self._properties[val] = match.groups()[0]                

            except Exception as e:
                print("ERROR! unable to get clip properties!", e)
            
            KNOWN_PROPERTIES[self.id] = self._properties
    
        return self._properties
            
        
class DataModel(object):
    def __init__(self, doc=None):
        if doc == None:
            doc = self.new_doc()
        self.doc = doc
        
        self.history = [doc.toxml()]
        self.redo_list = []
        self.last_saved_pos = 0
        
        self.view_widgets = []
        self.signaller = QtCore.QObject()
        self._clip_id = 0
        
    def new_doc(self):
        doc = minidom.Document()
        top_level = doc.createElement("q_auteur")
        doc.appendChild(top_level)
    
        return doc

    def new_workspace(self):
        self.doc = self.new_doc()
        self.get_max_clip_id()
        self.history = [self.xml]
        self.last_saved_pos = 0
        self.update_views()        
    
    def load(self, filename):
        self.doc = minidom.parse(filename)
        self.get_max_clip_id()
        self.history = [self.xml]
        self.last_saved_pos = 0
        self.update_views()
        
    def get_max_clip_id(self):
        self._clip_id = 0
        for node in self.doc.getElementsByTagName("clip"):
            id = node.getAttribute("id")
            if id:
                i = int(id)
                if i > self._clip_id:
                    self._clip_id = i
    
    def attach_view(self, view_widget):
        assert view_widget.update_data
        self.view_widgets.append(view_widget)
    
    @property
    def undo_available(self):
        return len(self.history) > 1
    
    @property
    def redo_available(self):
        return len(self.redo_list) > 0
        
    @property
    def next_clip_id(self):
        '''
        a counter to ensure each clip is uniquely id'd
        '''
        self._clip_id += 1
        return "%04d"% self._clip_id
    
    @property
    def is_dirty(self):
        try:
            return self.history[self.last_saved_pos] != self.xml
        except IndexError:
            return True
        
    @property
    def has_changed(self):
        return self.history[-1] != self.xml
    
    @property
    def tempfile(self):
        '''
        save a temporary file and return it's path
        '''
        temp = tempfile.mktemp(suffix=".xml")
        self.save_file(temp)
        return temp
    
    def save_file(self, filename):
        try:
            f = open(filename, "w")
            f.write(self.xml)
            f.close()
            self.last_saved_pos = len(self.history)-1
            return (True, "")
        except Exception as e:
            return (False, e)
    
    def update_history(self):
        if self.has_changed:
            self.history.append(self.xml)
            self.update_views()
            
    @property
    def has_sources(self):
        return not self.source_files == []
    
    def update_views(self):
        for view_widget in self.view_widgets:
            view_widget.update_data()
            
    def add_source_file(self, filename):
        '''
        see if we already have a source node with this filename
        if we do.. all is well. otherwise - create it.
        schema is as follows
        <sourceFile>
            <location>/~/Videos/examples.ogv</location>
            <clip/>
        </sourceFile>
        '''
        if filename in self.source_files:
            return
            
        source_node = self.doc.createElement("sourceFile")
        
        location_node = self.doc.createElement("location")
        text_node = self.doc.createTextNode(filename)
        location_node.appendChild(text_node)
        
        clip_node = self.doc.createElement("clip")
        clip_node.setAttribute("id", self.next_clip_id)
        
        source_node.appendChild(location_node)
        source_node.appendChild(clip_node)
        
        self.doc.documentElement.appendChild(source_node)
        
        self.update_history()
    
    def clip_from_selected(self, selected_clip):
        '''
        returns a tuple, (source, clip)
        '''
        source_node = self.source_node_from_filename(selected_clip.source_file)
    
        clips = source_node.getElementsByTagName("clip")
        clip_node = None
        for clip in clips:
            if clip.getAttribute("id") == selected_clip.id:
                clip_node = clip
        
        return source_node, clip_node
    
    def split_clip(self, selected_clip, time_stamp):
        print("splitting clip", selected_clip, time_stamp)
        
        source_node, current_node = self.clip_from_selected(selected_clip)
        
        current_node.setAttribute("end", "%.1f"% time_stamp)
        
        new_clip_id = self.next_clip_id
        new_clip_node = self.doc.createElement("clip")
        new_clip_node.setAttribute("id", new_clip_id)
        new_clip_node.setAttribute("start", "%.1f"% time_stamp)
        
        source_node.appendChild(new_clip_node)
        
        self.update_history()
        return new_clip_id
    
    def delete_clip(self, selected_clip):
        '''
        deletes the selected clip
        '''
        print("deleting clip", selected_clip) 
        source_node, clip_node = self.clip_from_selected(selected_clip)
        print(source_node.removeChild(clip_node))
        self.update_history()
    
    def source_node_from_filename(self, filename):
        '''
        returns the source_file node for a known filename
        '''
        l = []
        for source_node in self.doc.getElementsByTagName("sourceFile"):
            for location_node in source_node.getElementsByTagName("location"):
                location_text = location_node.firstChild.data.strip()
                if location_text == filename:
                    return source_node
    
    def source_location_from_clipnode(self, clipnode):
        '''
        returns the file location for a known clipnode
        '''
        source_node = clipnode.parentNode
        location_node = source_node.getElementsByTagName("location")[0]
        location_text = location_node.firstChild.data.strip()
        return location_text
            
    @property
    def source_files(self):
        '''
        returns a list of source_file locations
        eg. ["/home/user/Videos/holiday1.avi", ...]
        '''
        l = []
        for source_node in self.doc.getElementsByTagName("sourceFile"):
            for location_node in source_node.getElementsByTagName("location"):
                location_text = location_node.firstChild.data
                l.append(location_text.strip())
        return l
        
    @property
    def clips(self):
        '''
        returns a list of clips (in order!)
        '''
        clips = []
        for clip_node in self.doc.getElementsByTagName("clip"):
            clip = Clip()
            clip.source_file = self.source_location_from_clipnode(clip_node)
            clips.append(clip)
            clip._id = clip_node.getAttribute("id")
            clip._start = clip_node.getAttribute("start")
            clip._end = clip_node.getAttribute("end")
            
        return clips
        
    @property
    def xml(self):
        '''
        returns the current xml
        '''
        return self.doc.toxml()
    
    @property
    def pretty_xml(self):
        '''
        the underlying pretty xml
        '''
        return self.xml.replace(">",">\n").replace("\n</","</").\
        replace("</","\n</")
    
    def _restore_last_history(self):
        doc = minidom.parseString(self.history[-1])
        self.doc = doc
        self.update_history()
        self.update_views()
        
    def undo(self):
        if self.undo_available:
            self.redo_list.append(self.history.pop(-1))
            self._restore_last_history()
            
    def redo(self):
        if self.redo_available:
            self.history.append(self.redo_list.pop(-1))
            self._restore_last_history()
            
    @property
    def log_widget_text(self):
        '''
        the text supplied to the log_widget
        '''
        return self.pretty_xml
    

KNOWN_IMAGES, KNOWN_PROPERTIES = {}, {}

def _test_model():
    import gettext
    gettext.install("auteur")
    
    import sys
    sys.path.insert(0, "../../")
    from lib_q_auteur.components.images import q_resources
    
    obj = DataModel() 
    
    obj.add_source_file("/home/neil/Videos/Frasier xmas.flv  ")    
    print(obj.source_files)
    print("file duration %s"% obj.clips[0].length)
    print(obj.xml)
    print("TEST CTRL-X")
    obj.undo()
    print(obj.source_files)
    
if __name__ == "__main__":
    _test_model()
